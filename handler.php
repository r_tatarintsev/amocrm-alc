<?php
$subdomain = '';
$user = array(
	'USER_LOGIN' => '',
	'USER_HASH' => ''
);
$data = array(
	'name' => isset($_POST['name']) ? $_POST['name'] : '',
	'company' => isset($_POST['company']) ? $_POST['company'] : '',
	'phone' => isset($_POST['phone']) ? $_POST['phone'] : '',
	'email' => isset($_POST['email']) ? $_POST['email'] : '',
	'session_id' => isset($_POST['session_id']) ? $_POST['session_id'] : '',
	'comment' => isset($_POST['comment']) ? $_POST['comment'] : '',
	'type' => isset($_POST['type']) ? $_POST['type'] : '',
	'family' => isset($_POST['family']) ? $_POST['family'] : '',
);

run ($subdomain, $user, $data, $modx);
return true;


function Import()
{
	require('import.php');
	foreach ($arr as $c) {
		$data = array(
			'name' 		=> isset($c[1]) ? $c[1] : '',
			'phone' 	=> isset($c[2]) ? $c[2] : '',
			'email' 	=> isset($c[3]) ? $c[3] : '',
			'session' 	=> isset($c[4]) ? $c[4] : '',
			'comment' 	=> isset($c['comment']) ? $c['comment'] : ''
		);
	}
	return $data;

}

function CheckCurlResponse($code)
{
	static $iteration = 0;
	$iteration++;
	$code   = (int) $code;
	$errors = array(
		301 => 'Moved permanently',
		400 => 'Bad request',
		401 => 'Unauthorized',
		403 => 'Forbidden',
		404 => 'Not found',
		500 => 'Internal server error',
		502 => 'Bad gateway',
		503 => 'Service unavailable'
	);
	try {
		#Если код ответа не равен 200 или 204 - возвращаем сообщение об ошибке
		if ($code != 200 && $code != 204)
			throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
	}
	catch (Exception $E) {
		echo $iteration;
		die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());

	}
}

function Request($link, $param = false)
{
	$curl = curl_init();
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
	curl_setopt($curl, CURLOPT_URL, $link);
	if ($param) {
		curl_setopt($curl, CURLOPT_POST, true);
		curl_setopt($curl, CURLOPT_POSTFIELDS, $param);
	}
	curl_setopt($curl, CURLOPT_HEADER, false);
	curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
	curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
	curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	
	$out  = curl_exec($curl); #Инициируем запрос к API и сохраняем ответ в переменную
	$code = curl_getinfo($curl, CURLINFO_HTTP_CODE); #Получим HTTP-код ответа сервера
	curl_close($curl); #Заверашем сеанс cURL
	CheckCurlResponse($code);
	$out = ($out) ? json_decode($out, true) : false;
	return $out;
}

function run ($subdomain, $user ,$data, $modx) {

	/**************************************************
	 ******************* Авторизация ******************
	 *************************************************/

	$link     = 'https://' . $subdomain . '.amocrm.ru/private/api/auth.php?type=json';
	$param    = http_build_query($user);
	$response = Request($link, $param);
	$response = $response['response'];

	/**************************************************
	 ************* Информация об аккаунте *************
	 *************************************************/

	$link     = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/accounts/current';
	$response = Request($link);
	$account  = $response['response']['account'];

	/**************************************************
	 *************** Информация о полях ***************
	 *************************************************/

	$need = array_flip(array(
		'POSITION',
		'PHONE',
		'EMAIL'
	));
	if (isset($account['custom_fields'], $account['custom_fields']['contacts']))
		do {
			foreach ($account['custom_fields']['contacts'] as $field)
				if (is_array($field) && isset($field['id'])) {
					if (isset($field['code']) && isset($need[$field['code']]))
						$fields[$field['code']] = (int) $field['id'];
					
					$diff = array_diff_key($need, $fields);
					if (empty($diff))
						break 2;
				}
			if (isset($diff))
				die('В amoCRM отсутствуют следующие поля' . ': ' . join(', ', $diff));
			else
				die('Невозможно получить дополнительные поля');
		} while (false);
	else
		die('Невозможно получить дополнительные поля');
	$custom_fields = isset($fields) ? $fields : false;

	/**************************************************
	 *************** Добавление сделки ****************
	 *************************************************/
	
	if (isset($data['type']) && $data['type'] == 'free_book'){
		$set  = array(
			'name' => $data['name'],
			'status_id' => 14174707
		);
	} elseif (isset($data['family']) && $data['family'] !== ''){
	    $data['name'] = $data['family'];
    	$set  = array(
			'name' => $data['name'],
			'status_id' => 14174728
		);    
	} else {
		$data['session_id'] = explode('_',$data['session_id']);
		
		/**************************************************
		 $data['session_id'][0] - 44 (МСК) - 45(СПБ)
		 $data['session_id'][1] - 46,55 - Основная сессия
		 $data['session_id'][1] - 53,51 - 1-ая доп.сессия
		 $data['session_id'][1] - 54,56 - 2-ая доп.сессия
		 $data['session_id'][2] - id ресурса с сессией
		**************************************************/

		if ($data['session_id'][1] == 46 || $data['session_id'][1] == 55) $type_session = 277773; 	// Основная сессия
		if ($data['session_id'][1] == 53 || $data['session_id'][1] == 51) $type_session = 277789;	// 1-ая доп.сессия
		if ($data['session_id'][1] == 54 || $data['session_id'][1] == 56) $type_session = 277791; 	// 2-ая доп.сессия

		$resource  = $modx->getObject('modResource', $data['session_id'][2]);
		$pagetitle = strstr($resource->get('pagetitle'), '-');
		if (preg_match_all('#\d+#', $pagetitle, $matches)){
			$date_session = $matches[0][0].'.'.$matches[0][1].'.'.date("Y");
		}

		$set  = array(
			'name' => $data['name'],
			'custom_fields' => array(
				array(
					'id' => $type_session,
					'values' => array(
						array(
							'value' => $date_session
						)
					)
				),
				array(
					'id' => 419878,
					'values' => array(
						array(
							'value' => $data['comment']
						)
					)
				),
			)
		);
	}

	$leads['request']['leads']['add'][] = $set;
	$link                               = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/leads/set';
	$param                              = json_encode($leads);
	$response                           = Request($link, $param);
	$linked_leads_id                    = array();
	$linked_leads_id[]                  = $response['response']['leads']['add'][0]['id'];

	/**************************************************
	 ************* Информация о контактах *************
	 *************************************************/

	$link_email     = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/contacts/list?query=' . $data['email'];
	$link_name     = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/contacts/list?query=' . $data['name'];
	$response_email = Request($link_email);
	$response_name = Request($link_name); 
	 
	if ($response_email) {
		$update = $response_email['response']['contacts'][0]['id'];
		$update_leads = $response_email['response']['contacts'][0]['linked_leads_id'];
	}elseif ($response_name) {
		$update = $response_name['response']['contacts'][0]['id'];
		$update_leads = $response_name['response']['contacts'][0]['linked_leads_id'];
	}

	/**************************************************
	 *************** Добавление контакта **************
	 *************************************************/
    
	$contact = array(
		'name' => $data['name'],
		'linked_leads_id' => $linked_leads_id,
		'custom_fields' => array(
			array(
				'id' => $custom_fields['EMAIL'],
				'values' => array(
					array(
						'value' => $data['email'],
						'enum' => 'WORK'
					)
				)
			),
			array(
				'id' => 281427,
				'values' => array(
					array(
						'value' => 'повторники'
					)
				)
			)
		)
	);

	if (!empty($data['company']))
		$contact += array(
			'company_name' => $data['company']
		);

	if (!empty($data['phone']))
		$contact['custom_fields'][] = array(
			'id' => $custom_fields['PHONE'],
			'values' => array(
				array(
					'value' => $data['phone'],
					'enum' => 'OTHER'
				)
			)
		);

	if (isset($update)) {
		$update_leads[] = ''.$linked_leads_id[0];
		$contact = array(
			'id' => $update,
			'name' => $data['name'],
			'last_modified' => time(),
			'linked_leads_id' => $update_leads,
			'custom_fields' => array(
				array(
					'id' => 281427,
					'values' => array(
						array(
							'value' => 'повторники'
						)
					)
				)
			)
		);
		$con['request']['contacts']['update'][] = $contact;
	} else {
		$con['request']['contacts']['add'][] = $contact;
	}
	$link                                = 'https://' . $subdomain . '.amocrm.ru/private/api/v2/json/contacts/set';
	$param                               = json_encode($con);
	$response                            = Request($link, $param);
	$response                            = $response['response']['contacts']['add'];
}